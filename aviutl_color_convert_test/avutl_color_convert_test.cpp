﻿#include "filter.h"
#include <cstdint>
#include <iostream>
#include <atomic>
#include <thread>
#include <chrono>
namespace makki {
	namespace color_cvt {
		void rgb2yc(PIXEL_YC& re, const PIXEL& in) {
			re.y  = (( 4918 * in.r + 354) >> 10) + (( 9655 * in.g + 585) >> 10) + (( 1875 * in.b + 523) >> 10);
			re.cb = ((-2775 * in.r + 240) >> 10) + ((-5449 * in.g + 515) >> 10) + (( 8224 * in.b + 256) >> 10);
			re.cr = (( 8224 * in.r + 256) >> 10) + ((-6887 * in.g + 110) >> 10) + ((-1337 * in.b + 646) >> 10);
		}
	}
}
std::ostream& operator<<(std::ostream& os, const PIXEL& px) {
	os << "R:" << px.r << " G:" << px.g << " B:" << px.b;
	return os;
}
std::ostream& operator<<(std::ostream& os, const PIXEL_YC& px) {
	os << "Y:" << px.y << " Cb:" << px.cb << " Cr:" << px.cr;
	return os;
}
namespace aviutl_color_convert_test {
#ifdef _DEBUG
	static const char* plugin_name = "color_convert_test(DEBUG)";
#else
	static const char* plugin_name = "color_convert_test";
#endif
	static const char* plugin_version_str = "color_convert_test v1.0.0 by yumetodo";
	static std::atomic_bool notify_exit = false;
	static void test(EXFUNC* exfunc) {
		using namespace std::chrono_literals;
		FILE *fp;
		::AllocConsole();
		freopen_s(&fp, "CON", "w", stdout);    // 標準出力の割り当て

		try {
			std::cout << "start test" << std::endl;
			for (int r = 0; r < 255; ++r) for (int g = 0; g < 255; ++g) for (int b = 0; b < 255; ++b) {
				PIXEL in;
				in.r = static_cast<std::uint8_t>(r);
				in.g = static_cast<std::uint8_t>(g);
				in.b = static_cast<std::uint8_t>(b);
				PIXEL_YC re[2];
				makki::color_cvt::rgb2yc(re[0], in);
				exfunc->rgb2yc(&re[1], &in, 1);
				if (re[0].y != re[1].y || re[0].cb != re[1].cb || re[0].cb != re[1].cb) {
					std::cout
						<< "error: RGB(" << in << ")-> YC48" << std::endl
						<< "Makki:" << re[0] << std::endl
						<< "Aviutl: " << re[0] << std::endl;
				}
			}
			std::cout << "finish test!" << std::endl;
		}
		catch(...){}

		while (!notify_exit) std::this_thread::sleep_for(50ms);
		fclose(fp);
		::FreeConsole();
	}
	BOOL init(FILTER* fp) noexcept { 
		static bool test_started = false;
		if (!test_started) {
			if (nullptr == fp || nullptr == fp->exfunc) return FALSE;
			std::thread th(test, fp->exfunc);
			th.detach();
		}
		return TRUE; 
	}
	BOOL exit(FILTER* fp)
	{
		notify_exit = true;
		return TRUE;
	}
	static FILTER_DLL filter_info_en = {               // English UI filter info
		FILTER_FLAG_EX_INFORMATION | FILTER_FLAG_PRIORITY_LOWEST,	//	filter flags, use bitwise OR to add more
		0, 0,							//	dialogbox size
		const_cast<char*>(plugin_name),				//	Filter plugin name
		0,						//	トラックバーの数 (0なら名前初期値等もNULLでよい)
		nullptr,						//	slider label names in English
		nullptr,					//	トラックバーの初期値郡へのポインタ
		nullptr, nullptr,				//	トラックバーの数値の下限上限 (NULLなら全て0～256)
		0,						//	チェックボックスの数 (0なら名前初期値等もNULLでよい)
		nullptr,					//	チェックボックスの名前郡へのポインタ
		nullptr,					//	チェックボックスの初期値郡へのポインタ
		nullptr,		//	main filter function, use NULL to skip
		init,		//	initialization function, use NULL to skip
		exit,		//	on-exit function, use NULL to skip
		nullptr,		//	invokes when when settings changed. use NULL to skip
		nullptr,							//	for capturing dialog's control messages. Essential if you use button or auto uncheck some checkboxes.
		nullptr, nullptr,						//	Reserved. Do not use.
		nullptr,							//  pointer to extra data when FILTER_FLAG_EX_DATA is set
		0,							//  extra data size
		const_cast<char*>(plugin_version_str),
		//  pointer or c-string for full filter info when FILTER_FLAG_EX_INFORMATION is set.
		nullptr,	//	invoke just before saving starts. NULL to skip
		nullptr,	//	invoke just after saving ends. NULL to skip
	};
}

// Export the above filter table
EXTERN_C  __declspec(dllexport) FILTER_DLL** GetFilterTableList(void)
{
	//must terminate with nullptr
	//http://qiita.com/yumetodo/items/4d972da03b3be788fcda
	static FILTER_DLL* pluginlist[] = { &aviutl_color_convert_test::filter_info_en, nullptr };
	return pluginlist;
}
